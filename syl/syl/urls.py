"""syl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from course.search_view import course_index_search
from syl import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('user.urls')),  # 导包路径  依赖  sys.path
    path('verify/', include('verifications.urls')),
    path('oauth/', include('oauth.urls')),
    path('course/', include('course.urls')),
    path('goods/', include('goods.urls')),
    path('chat/', include('chat.urls')),
    # path('search/', include('haystack.urls')),  # 搜索
    path('search/', course_index_search),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
