"""
Django settings for syl project.

Generated by 'django-admin startproject' using Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import sys
import datetime
from rest_framework import ISO_8601

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 给python解释器,  把apps目录, 插入到系统的包搜索路径第一个
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ih9(@xv)tgbfs@o!u7=ac%sf7_#dct_v!x-5li0xjl^il4i95x'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# DEBUG = False

ALLOWED_HOSTS = ['mysyl.com','127.0.0.1', '*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'haystack',
    'corsheaders',
    'django_filters',
    'rest_framework',
    'user.apps.UserConfig',
    'verifications.apps.VerificationsConfig',
    'oauth.apps.OauthConfig',
    'course.apps.CourseConfig',
    'goods.apps.GoodsConfig',
    'chat.apps.ChatConfig'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'syl.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'syl.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'syl',
#         'HOST': '10.211.55.15',
#         'PORT': '3307',
#         'USER': 'root',
#         'PASSWORD': 'mysql',
#     }
# }

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'syl',
#         'HOST': '10.211.55.15',
#         'PORT': '3307',
#         'USER': 'root',
#         'PASSWORD': 'mysql',
#     },
#     'slave': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'syl',
#         'HOST': '10.211.55.15',
#         'PORT': '3308',
#         'USER': 'root',
#         'PASSWORD': 'mysql',
#     }
# }

# 数据库路由配置
# DATABASE_ROUTERS = ['utils.db_router.MasterSlaveDBRouter']

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'zh-hans'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# 注册自己的模型类   应用名.模型名
AUTH_USER_MODEL = 'user.User'

# CORS跨域请求白名单设置
CORS_ORIGIN_WHITELIST = (
    'http://127.0.0.1:8080',
    'http://localhost:8080',
    'http://127.0.0.1:8081',
    'http://localhost:8081',
    'http://0.0.0.0:8080',
    'http://mysyl.com:8080',
)
# 允许携带cookie
CORS_ALLOW_CREDENTIALS = True

# 过滤器
# 1,安装 django-filter
# 2,注册应用
# 3,配置settings, 在view里配置可过滤的字段
# 4,使用 查询字符串携带过滤信息

REST_FRAMEWORK = {
    # 文档报错： AttributeError: ‘AutoSchema’ object has no attribute ‘get_link’
    # 用下面的设置可以解决
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.AutoSchema',
    # 默认设置是:
    # 'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.openapi.AutoSchema',

    # 异常处理器
    # 'EXCEPTION_HANDLER': 'user.utils.exception_handler',

    # 用户登陆认证方式
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',  # 使用session时的认证器, drfweb页面的认证方式
        # 'rest_framework.authentication.BasicAuthentication'  # 提交表单时的认证器
    ],
    # 权限配置, 顺序靠上的严格
    'DEFAULT_PERMISSION_CLASSES': [
        # 'rest_framework.permissions.IsAdminUser',  # 管理员可以访问
        # 'rest_framework.permissions.IsAuthenticated',  # 认证用户可以访问
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',  # 认证用户可以访问, 否则只能读取
        # 'rest_framework.permissions.AllowAny',  # 所有用户都可以访问
    ],
    # 限流
    'DEFAULT_THROTTLE_CLASSES': [
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle',
    ],
    # 限流策略
    'DEFAULT_THROTTLE_RATES': {
        'user': '1000/hour',
        'anon': '300/day',
    },

    # 全局分页器, 例如 省市区的数据自定义分页器, 不需要分页
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    # 每页返回数量
    'PAGE_SIZE': 10,  # 默认 None

    # 过滤器后端
    'DEFAULT_FILTER_BACKENDS': [
        'django_filters.rest_framework.DjangoFilterBackend',
        # 'django_filters.rest_framework.backends.DjangoFilterBackend', 包路径有变化
    ],

    # Filtering 过滤排序
    'SEARCH_PARAM': 'search',
    'ORDERING_PARAM': 'ordering',

    # Authentication  认证
    # 未认证用户使用的用户类型
    'UNAUTHENTICATED_USER': 'django.contrib.auth.models.AnonymousUser',
    # 未认证用户使用的Token值
    'UNAUTHENTICATED_TOKEN': None,
}

# jwt载荷中的有效期设置
JWT_AUTH = {
    # headers中 Authorization 值的前缀
    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    # token 有效期
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'JWT_ALLOW_REFRESH': True,
    # token在24小时内过期, 可续期token
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(hours=24),
    # 自定义返回格式，需要手工创建
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'user.utils.jwt_response_payload_handler',
}

# 自定义验证后端
AUTHENTICATION_BACKENDS = ['django.contrib.auth.backends.ModelBackend', 'user.utils.EmailAuthBackend',
                           'user.utils.PhoneAuthBackend']

# 缓存配置
CACHES = {
    "default": {  # 默认存缓位置
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "session": {  # 存session
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    "verify_code": {  # 图形验证码,短信验证码
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    },
    # "write": {  # 主服务
    #     "BACKEND": "django_redis.cache.RedisCache",
    #     "LOCATION": "redis://10.211.55.15:6377/10",
    #     "OPTIONS": {
    #         "CLIENT_CLASS": "django_redis.client.DefaultClient",
    #     }
    # },
    # "read": {  # 从服务
    #     "BACKEND": "django_redis.cache.RedisCache",
    #     "LOCATION": "redis://10.211.55.15:6378/10",
    #     "OPTIONS": {
    #         "CLIENT_CLASS": "django_redis.client.DefaultClient",
    #     }
    # },
}

# redis集群配置
# CACHES = {
#     'default': {
#         'BACKEND': 'django_redis.cache.RedisCache',
#         'LOCATION': [
#             "redis://10.211.55.15:7000",
#             "redis://10.211.55.15:7001",
#             "redis://10.211.55.15:7002",
#         ],
#         'OPTIONS': {
#             'REDIS_CLIENT_CLASS': 'rediscluster.RedisCluster',
#             'CONNECTION_POOL_CLASS': 'rediscluster.connection.ClusterConnectionPool',
#             "CONNECTION_POOL_KWARGS": {"decode_responses": True}
#         }
#     }
# }

# 配置session使用redis存储
# SESSION_ENGINE = "django.contrib.sessions.backends.cache"
# 配置session存储的位置: 使用cache中的 session配置
# SESSION_CACHE_ALIAS = "session"

# 支付宝配置
ALIPAY_APPID = '2016080100142469'
ALIPAY_DEBUG = True
ALIPAY_URL = 'https://openapi.alipaydev.com/gateway.do'
ALIPAY_RETURN_URL = 'http://mysyl.com:8080/payment/callback/'

# 七牛相关参数设置
# try:
#     QINIU_ACCESS_KEY = "dkwbDQSG3K4Szvy4R121Tnv0EVyHLp_qKf5Ke1GK"
#     QINIU_SECRET_KEY = "VjGgmSncDlUS7SPr--JeX_nPNJpVKayaBQnYySTI"
#     QINIU_BUCKET_NAME = "syl1908"
#     QINIU_BUCKET_DOMAIN = "qfnfcqki0.hn-bkt.clouddn.com"
#     QINIU_SECURE_URL = False  # 使用http
#     PREFIX_URL = 'http://'
#     MEDIA_URL = PREFIX_URL + QINIU_BUCKET_DOMAIN + '/media/'
#     DEFAULT_FILE_STORAGE = 'qiniustorage.backends.QiniuMediaStorage'
#     MEDIA_ROOT = os.path.join('', "media")
# except Exception as e:
#     print(e)

# MEDIA_URL = "/media/"
# MEDIA_ROOT = os.path.join(BASE_DIR, "media")
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

# 全文检索配置
HAYSTACK_SEARCH_RESULTS_PER_PAGE = 15
# whoosh引擎
HAYSTACK_CONNECTIONS = {
    'default': {
        # 'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'ENGINE': 'course.whoosh_cn_backend.MyWhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
    },
}

# ES引擎
# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
#         'URL': 'http://10.211.55.15:9200/',  # Elasticsearch服务器ip地址，端口号固定为9200
#         'INDEX_NAME': 'syl',  # Elasticsearch建立的索引库的名称
#     },
# }

# 当添加、修改、删除数据时，自动生成索引
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# fasfdfs 服务器地址
# FDFS_BASE_URL = 'http://10.211.55.15:8888/'
# FDFS_CONF_PATH = os.path.join(BASE_DIR, 'utils/fdfs/client.conf')
#
# # 修改系统默认的文件管理类
# DEFAULT_FILE_STORAGE = 'utils.fdfs.my_storage.MyStorage'


# 静态资源收集位置
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')