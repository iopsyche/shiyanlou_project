from django.db import models
from django.contrib.auth.models import AbstractUser
from utils.MyBaseModel import Base


# Create your models here.


class Vip(Base):
    title = models.CharField('vip名称', max_length=16)
    level = models.CharField('vip等级', max_length=16)
    desc = models.CharField('vip描述', max_length=255)
    period = models.IntegerField('有效期', default=365)

    class Meta:
        db_table = 'tb_vip'

    def __str__(self):
        return "%s-%s" % (self.title, self.level)


class User(AbstractUser):
    phone = models.CharField('手机号', max_length=11)
    img = models.ImageField(upload_to='user', null=True)
    nick_name = models.CharField('昵称', max_length=20)
    address = models.CharField('地址', max_length=255)
    vip = models.ForeignKey(Vip, on_delete=models.SET_NULL, default=None, null=True)
    vip_expiration = models.DateField('vip到期时间', blank=True, default=None, null=True)

    class Meta:
        db_table = 'tb_user'
