from django.urls import include, path
from rest_framework.authtoken.views import obtain_auth_token

from user import views
from rest_framework.routers import SimpleRouter, DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

# 自动生成路由方法, 必须使用视图集
# router = SimpleRouter()  # 没有根路由  /user/ 无法识别
router = DefaultRouter()  # 有根路由
router.register(r'user', views.UserViewSet)

urlpatterns = [
    path('count/', views.RegCountView.as_view()),  # 查询用户名手机号使用量的视图,  /user/count/
    path('register/', views.RegisterView.as_view()),  # 注册视图,  /user/register/
    path('index/', views.index),  # 函数视图
    path('class_index/', views.IndexView.as_view()),  # 普通类视图
    path('api_index/', views.ApiView.as_view()),  # APIView类视图
    path('login/', obtain_jwt_token),  # 获取token
    path('refresh/', refresh_jwt_token),  # 刷新token
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),  # 认证地址
]

urlpatterns += router.urls  # 模块地址
# print(router.urls)
