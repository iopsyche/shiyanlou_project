from django.contrib import admin

# Register your models here.
from . import models

admin.site.register(models.Course)
admin.site.register(models.Sections)
admin.site.register(models.Chapters)
admin.site.register(models.CourseType)
admin.site.register(models.CourseTag)
admin.site.register(models.Path)
admin.site.register(models.JieDuan)
admin.site.register(models.UserCourse)
