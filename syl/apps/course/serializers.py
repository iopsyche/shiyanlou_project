from rest_framework import serializers

from course.models import Course, CourseType, CourseTag, Chapters, Sections, Follow, Path, JieDuan, UserCourse, \
    UserSections, CommentReply, Comment
from user.models import User


class CourseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseType
        fields = '__all__'


class CourseTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseTag
        fields = '__all__'


class SectionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sections
        fields = '__all__'


class ChaptersSerializer(serializers.ModelSerializer):
    sections = SectionsSerializer(many=True)

    class Meta:
        model = Chapters
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'  # 所有字段


class CourseDeepSerializer(CourseSerializer):
    # 字段名名, 必须是模型可以 . 引用到的变量
    # Course().   "chapters"  才能作为字段名,  如果是集合, 需要加many=True,

    chapters = ChaptersSerializer(many=True)


class FollowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Follow
        fields = '__all__'


class JieDuanSerializer(serializers.ModelSerializer):
    courses = CourseSerializer(many=True)

    class Meta:
        model = JieDuan
        fields = '__all__'


class PathSerializer(serializers.ModelSerializer):
    class Meta:
        model = Path
        fields = ('id', 'title', 'img', 'desc', 'course_total')
        # fields = '__all__'
        # exclude = ['user']


class PathDeepSerializer(serializers.ModelSerializer):
    jieduan = JieDuanSerializer(many=True)

    class Meta:
        model = Path
        # fields = '__all__'
        exclude = ['user']


class UserCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCourse
        fields = '__all__'
        # exclude = ['user']


class UserSectionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserSections
        fields = '__all__'
        # exclude = ['user']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'img']
        # exclude = ['user']


class CommentReplySerializer(serializers.ModelSerializer):
    user = UserSerializer()
    to_user = UserSerializer()

    class Meta:
        model = CommentReply
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):
    reply = CommentReplySerializer(many=True, read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'