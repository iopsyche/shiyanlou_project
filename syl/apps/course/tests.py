import os
import time

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'syl.settings')
django.setup()

from django.core.cache import cache

def getdata():
    # 获取一个数据..
    # 1. 查询缓存中有没有
    # 1.1 如果没有, 就生成数据, 并保存在缓存中
    data = cache.get('data')
    if data:
        return data
    else:
        data = '123'
        time.sleep(5)
        cache.set('data', data, 20)
        return data


if __name__ == '__main__':
    print(getdata())
