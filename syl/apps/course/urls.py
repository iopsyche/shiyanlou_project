from django.urls import path, re_path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()  # 有根路由
router.register(r'course', views.CourseViewSet)
router.register(r'type', views.CourseTypeViewSet)
router.register(r'tag', views.CourseTagViewSet)
router.register(r'chapters', views.ChaptersViewSet)
router.register(r'sections', views.SectionsViewSet)
router.register(r'follow', views.FollowViewSet)
router.register(r'path', views.PathViewSet)
router.register(r'jieduan', views.JieDuanViewSet)
router.register(r'user_sections', views.UserSectionsViewSet)
router.register(r'comment', views.CommentViewSet)
router.register(r'commentreply', views.CommentReplyViewSet)

urlpatterns = [
    path('addfollow/', views.AddFollow.as_view()),
    path('addplayhistory/', views.AddPlayHistory.as_view()),
    path('recocourse/', views.RecoCourse.as_view()),
    path('commentself/', views.CommentSelfView.as_view()),
]

urlpatterns += router.urls
