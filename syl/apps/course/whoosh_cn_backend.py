from haystack.backends.whoosh_backend import WhooshEngine, WhooshSearchBackend

from whoosh.fields import TEXT

from jieba.analyse import ChineseAnalyzer


class MyWhooshSearchBackend(WhooshSearchBackend):
    def build_schema(self, fields):
        (content_field_name, schema) = super().build_schema(fields)
        schema._fields['text'] = TEXT(stored=True,
                                      analyzer=ChineseAnalyzer(),
                                      field_boost=fields.get('text').boost,
                                      sortable=True)

        return (content_field_name, schema)


class MyWhooshEngine(WhooshEngine):
    backend = MyWhooshSearchBackend
