from django.db import models
from utils.MyBaseModel import Base


class CourseType(Base):
    title = models.CharField('课程类别', max_length=16)
    sequence = models.IntegerField('展示顺序', default=10)

    class Meta:
        db_table = 'tb_coursetype'

    def __str__(self):
        return self.title


class CourseTag(Base):
    title = models.CharField('课程标签', max_length=16)
    sequence = models.IntegerField('展示顺序', default=10)

    class Meta:
        db_table = 'tb_coursetag'

    def __str__(self):
        return self.title


class Course(Base):
    STATUS = (
        ('0', '即将上线'),
        ('1', '已上线'),
        ('2', '已下线'),
    )

    title = models.CharField('课程名', max_length=24)
    desc = models.CharField('课程描述', max_length=256)
    img = models.ImageField('课程logo', upload_to='course', null=True)
    course_type = models.ForeignKey(CourseType, verbose_name='课程类型', on_delete=models.SET_NULL, default=None, null=True)
    course_tag = models.ManyToManyField(CourseTag, verbose_name='课程标签')
    status = models.CharField('课程状态', choices=STATUS, max_length=8, default='1')
    follower = models.IntegerField('关注人数', default=0)
    learner = models.IntegerField('学习人数', default=0)

    class Meta:
        db_table = 'tb_course'

    def __str__(self):
        return self.title


class Chapters(Base):
    title = models.CharField('章标题', max_length=24)
    serial_num = models.IntegerField('章序号')
    course = models.ForeignKey(Course, related_name='chapters', on_delete=models.SET_NULL, null=True)

    class Meta:
        db_table = 'tb_chapters'

    def __str__(self):
        return self.title


class Sections(Base):
    title = models.CharField('节标题', max_length=24)
    serial_num = models.IntegerField('节序号')
    chapters = models.ForeignKey(Chapters, related_name='sections', on_delete=models.SET_NULL, null=True)
    learn_time = models.IntegerField('学习小时', default=1)
    video = models.FileField("上传视频", upload_to="videos/%Y%m%d/", blank=True, max_length=1024)
    seq_num = models.IntegerField("序号", default=1)

    class Meta:
        db_table = 'tb_sections'

    def __str__(self):
        return self.title


class Follow(Base):
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    class Meta:
        db_table = 'tb_follow'


class Path(Base):
    title = models.CharField('路径名', max_length=16)
    img = models.ImageField('路径图片', upload_to='path', null=True)
    desc = models.CharField('路径描述', max_length=255)
    user = models.ManyToManyField('user.User', blank=True)

    def course_total(self):
        count = 0
        for jd in self.jieduan.all():
            count = count + jd.courses.count()
        return count

    class Meta:
        db_table = 'tb_path'

    def __str__(self):
        return self.title


class JieDuan(Base):
    title = models.CharField('阶段名', max_length=16)
    serial_num = models.IntegerField('阶段序号')
    path = models.ForeignKey(Path, related_name='jieduan', on_delete=models.SET_NULL, null=True)
    courses = models.ManyToManyField(Course, blank=True)

    class Meta:
        db_table = 'tb_jieduan'

    def __str__(self):
        return "%s-第%s阶段-%s" % (self.path.title, self.serial_num, self.title)


class UserCourse(Base):
    """
    用户购买的课程
    """
    user = models.ForeignKey('user.User', on_delete=models.CASCADE, related_name='paycourse')
    course = models.ForeignKey('course.Course', on_delete=models.CASCADE, related_name='payuser')

    class Meta:
        db_table = 'tb_usercourse'

    def __str__(self):
        return "用户:%s, 课程:%s" % (self.user.username, self.course.title)


class UserSections(Base):
    user = models.ForeignKey('user.User', on_delete=models.PROTECT, related_name='usersections')
    course = models.ForeignKey(Course, on_delete=models.PROTECT, related_name='usersections')
    section = models.ForeignKey(Sections, on_delete=models.PROTECT, related_name='usersections')

    class Meta:
        db_table = 'tb_usersections'

    def __str__(self):
        return "用户:%s, 课程:%s, 小节: %s" % (self.user.username, self.course.title, self.sections.title)


# 评论
class Comment(Base):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course_comment')
    user = models.ForeignKey('user.User', on_delete=models.SET_NULL, null=True, related_name='user_comment')
    content = models.CharField(max_length=500)

    class Meta:
        db_table = "tb_comment"


# 评论
#   A: 123
#   B:A  234
# 评论的回复
class CommentReply(Base):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, related_name='reply')
    user = models.ForeignKey('user.User', on_delete=models.SET_NULL, null=True, related_name='comment')
    to_user = models.ForeignKey('user.User', on_delete=models.SET_NULL, null=True, related_name='comment_to')
    # replay_id = models.IntegerField('评论回复的id', default=None, null=True)
    # comment_reply = models.ForeignKey('self', on_delete=models.CASCADE, default=None, null=True,
    #                                   related_name='commentreply')
    content = models.CharField(max_length=500)

    class Meta:
        db_table = "tb_commentreply"


class CommentSelf(Base):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course_commentself')
    user = models.ForeignKey('user.User', on_delete=models.SET_NULL, null=True, related_name='user_commentself')
    content = models.CharField(max_length=500)
    father = models.ForeignKey('self', on_delete=models.CASCADE, related_name='children', default=None, null=True)

    class Meta:
        db_table = "tb_commentself"
