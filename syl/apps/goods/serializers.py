from rest_framework import serializers

from goods.models import GoodsCourse, Goods
from user.models import User


class GoodsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goods
        # fields = ('id', )
        fields = '__all__'  # 所有字段
        # exclude = ['password']  # 排除 id 字段
