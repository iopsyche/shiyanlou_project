from django.urls import include, path
from rest_framework.authtoken.views import obtain_auth_token

from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()  # 有根路由
router.register(r'goods', views.GoodsViewSet)  # /goods/goods/?
# router.register(r'user', views.UserViewSet)

urlpatterns = [
    path('getpayurl/', views.PayUrlView.as_view()),  # /goods/getpayurl/
    path('peyment/', views.PayMentView.as_view()),  # /goods/peyment/
    path('peyment2/', views.PayMent2View.as_view())  # /goods/peyment/
]

urlpatterns += router.urls  # 模块地址
# print(router.urls)
