import os

from alipay import AliPay
from syl import settings

# 获取文件路径
app_private_key_path = os.path.join(settings.BASE_DIR, "apps/goods/keys/app_private_key.pem")
alipay_public_key_path = os.path.join(settings.BASE_DIR, "apps/goods/keys/alipay_public_key.pem")

with open(app_private_key_path) as f:
    app_private_key_string = f.read()

with open(alipay_public_key_path) as f:
    alipay_public_key_string = f.read()

# print(app_private_key_string)
# print(alipay_public_key_string)

# 3, 处理业务
# 创建支付宝支付对象
alipay = AliPay(
    appid=settings.ALIPAY_APPID,
    app_notify_url=None,  # 默认回调url
    app_private_key_string=app_private_key_string,
    alipay_public_key_string=alipay_public_key_string,
    sign_type="RSA2",
    debug=settings.ALIPAY_DEBUG
)

# 生成登录支付宝连接
order_string = alipay.api_alipay_trade_page_pay(
    out_trade_no='SYL202008241212120000005',
    total_amount=str(199),
    subject="实验楼订单号:%s" % 'SYL202008241212120000005',
    return_url=settings.ALIPAY_RETURN_URL,
)

# 响应登录支付宝连接
# 真实环境电脑网站支付网关：https://openapi.alipay.com/gateway.do? + order_string
# 沙箱环境电脑网站支付网关：https://openapi.alipaydev.com/gateway.do? + order_string
alipay_url = settings.ALIPAY_URL + "?" + order_string
# print(alipay_url)

"""
http://mysyl.com:8080/payment/callback/?
charset=utf-8
&out_trade_no=SYL202008241212120000003
&method=alipay.trade.page.pay.return
&total_amount=299.00
&sign=d%2FyEu073LUQ%2Fbvm%2BsOUbekAcTHkSx5M6TReEEs4PUUiU5XJd3o4TvvZ8nFbYAR17LTbK2%2BdBX22qbFnXrkH7ieVH6coUAlLM%2Fjm0bKu867JKL1m73QeXfEy62iaZ%2FcX%2BgbZX4e%2FXS39U3NdEVNwnZYUfmxShw%2FB0fkyQdb2BHvllOohLLgrc4SvnDYGY6YmxsvmiXzQXtAtEx1l4FnGAUnR%2F058QNPS3kcOpb8Pt%2FhAxhwIViLgT4tqHiMGRrexfIx2fdGVEfy0PJnI9%2FXGRYzj2bM2Kjv3aDeutjcHJigQ0Qi1h9E3BFi76OEhuW2UTjJ4Q%2BWLk4C1xr4mL%2BbKYPw%3D%3D
&trade_no=2020082422001494010500607431
&auth_app_id=2016080100142469
&version=1.0
&app_id=2016080100142469
&sign_type=RSA2
&seller_id=2088102169475485
&timestamp=2020-08-24%2017%3A38%3A18"""

data = {
    "charset": "utf-8",
    "out_trade_no": "SYL202008241212120000003",
    "method": "alipay.trade.page.pay.return",
    "total_amount": "299.00",
    "trade_no": "2020082422001494010500607431",
    "auth_app_id": "2016080100142469",
    "version": "1.0",
    "app_id": "2016080100142469",
    "sign_type": "RSA2",
    "seller_id": "2088102169475485",
    "timestamp": "2020-08-24%2017%3A38%3A18"
}
sign = "d%2FyEu073LUQ%2Fbvm%2BsOUbekAcTHkSx5M6TReEEs4PUUiU5XJd3o4TvvZ8nFbYAR17LTbK2%2BdBX22qbFnXrkH7ieVH6coUAlLM%2Fjm0bKu867JKL1m73QeXfEy62iaZ%2FcX%2BgbZX4e%2FXS39U3NdEVNwnZYUfmxShw%2FB0fkyQdb2BHvllOohLLgrc4SvnDYGY6YmxsvmiXzQXtAtEx1l4FnGAUnR%2F058QNPS3kcOpb8Pt%2FhAxhwIViLgT4tqHiMGRrexfIx2fdGVEfy0PJnI9%2FXGRYzj2bM2Kjv3aDeutjcHJigQ0Qi1h9E3BFi76OEhuW2UTjJ4Q%2BWLk4C1xr4mL%2BbKYPw%3D%3D"

result = alipay.verify(data, sign)
print(result)
