from django.db import models

# Create your models here.
from utils.MyBaseModel import Base


class Goods(Base):
    GOODS_TYPE = (
        ('1', 'Vip'),
        ('2', 'Course')
    )
    CHANNEL_TYPE = (
        ('1', '普通'),
        ('2', '促销')
    )
    goods_type = models.CharField('商品种类', choices=GOODS_TYPE, max_length=8)
    product_id = models.CharField('产品id', max_length=8)
    title = models.CharField('商品名称', max_length=24)
    price = models.DecimalField('商品价格', max_digits=8, decimal_places=2)
    channel_type = models.CharField('购买渠道', choices=CHANNEL_TYPE, max_length=8)
    period = models.IntegerField('有效期', default=365)

    is_launched = models.BooleanField('是否上架', default=True)

    class Meta:
        db_table = 'tb_goods'

    def __str__(self):
        return self.title


class GoodsCourse(Base):
    PAY_TYPE = (
        ('1', '普通'),
        ('2', '促销')
    )
    course = models.ForeignKey('course.Course', on_delete=models.PROTECT)
    title = models.CharField('商品名称', max_length=24)
    pay_type = models.CharField('购买渠道', choices=PAY_TYPE, max_length=8)
    price = models.DecimalField('商品价格', max_digits=8, decimal_places=2)
    period = models.IntegerField('有效期', default=365)
    is_launched = models.BooleanField('是否上架', default=True)

    class Meta:
        db_table = 'tb_goodscourse'

    def __str__(self):
        return self.title


class Orders(Base):
    PAY_METHOD = (
        (1, "支付宝"),
    )
    ORDER_STATUS = (
        (1, "待支付"),
        (2, "已支付"),
        (3, "已取消"),
    )
    user = models.ForeignKey('user.User', on_delete=models.PROTECT, verbose_name="下单用户")
    goods = models.ForeignKey(Goods, on_delete=models.PROTECT)
    order_id = models.CharField('订单号', max_length=24)
    trade_no = models.CharField('支付宝订单号', max_length=32, null=True)  # 28位
    pay_time = models.DateTimeField('支付时间', null=True)
    pay_method = models.CharField('支付方式', choices=PAY_METHOD, default=1, max_length=8)
    status = models.CharField('支付状态', choices=ORDER_STATUS, default=1, max_length=8)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="商品总金额")

    # 优惠券
    # 收货地址
    # ...

    class Meta:
        db_table = 'tb_orders'

    def __str__(self):
        return self.order_id
