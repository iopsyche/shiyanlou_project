from django.urls import path, re_path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    # websocket
    # path('websocket_test/', TemplateView.as_view(template_name='socket.html')),
    # path('websocket_push_test/', TemplateView.as_view(template_name='socket_push.html')),
    path('echo/', views.echo),
    path('push/', views.pushws),
    # path('test_socket', views.test_socket),
    # path('test_websocket', views.test_websocket),
    # path('path/', views.path),
]
