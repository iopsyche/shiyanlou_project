from django.urls import path, re_path
from . import views

urlpatterns = [
    path('weibo/', views.OauthWeibo.as_view()),  # /oauth/weibo/
    path('weibo/callback/', views.OauthWeiboCallback.as_view()),  # /oauth/weibo/callback/
    path('weibo/binduser/', views.OauthWeiboBindUser.as_view())  # /oauth/weibo/callback/
]