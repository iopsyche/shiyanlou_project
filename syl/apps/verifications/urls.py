from django.urls import path
from . import views

urlpatterns = [
    path('image_codes/', views.ImageCodeView.as_view()),  # /verify/image_codes/?uuid=xxx
    path('sms_codes/', views.SmsCodeView.as_view()),
]
