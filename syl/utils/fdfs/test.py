from fdfs_client.client import get_tracker_conf, Fdfs_client

from syl.settings import FDFS_CONF_PATH

conf = get_tracker_conf(conf_path=FDFS_CONF_PATH)
print(conf)
# 2. 创建FastDFS客户端实例
client = Fdfs_client(conf)
# 3. 调用FastDFS客户端上传文件方法
ret = client.upload_by_filename('/Users/mac/Pictures/0.jpg')
print(ret)
# ret = client.upload_by_buffer(content)
