from django.db import models


class Base(models.Model):
    create_time = models.DateTimeField('创建时间', auto_now_add=True, null=True)
    update_time = models.DateTimeField('更新时间', auto_now=True, null=True)

    class Meta:
        abstract = True
