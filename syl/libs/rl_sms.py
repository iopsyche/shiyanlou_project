import json

from ronglian_sms_sdk import SmsSDK

accId = '8aaf07086c8a1560016caed7857019b3'
accToken = '7c4f697e511b40d8bb284d9014b874c5'
appId = '8aaf07086c8a1560016caed785bd19b9'


def send_message(phone, datas):
    sdk = SmsSDK(accId, accToken, appId)
    tid = '1'  # 测试模板id为: 1. 内容为: 【云通讯】您的验证码是{1}，请于{2}分钟内正确输入。
    # mobile = '13303479527'
    # datas = ('666777', '5')  # 模板中的参数按照位置传递
    resp = sdk.sendMessage(tid, phone, datas)
    resp_dict = json.loads(resp)
    if resp_dict.get('statusCode') == '000000':
        return '0'
    return '-1'


if __name__ == '__main__':
    print(send_message('13303479527', ('1234', '5')))
