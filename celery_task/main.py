import os
import sys
from celery import Celery

# 执行celery命令时, 需要进入CELERY_BASE_DIR目录执行.
# celery项目中的所有导包地址, 都是以CELERY_BASE_DIR为基准设定.

CELERY_BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, CELERY_BASE_DIR)  # 增加导包路径
# 定义celery实例, 需要的参数, 1, 实例名,
app = Celery('syl_celery')

# 从配置文件中读取配置
app.config_from_object('config')

# 自动发现任务
app.autodiscover_tasks(['tasks_sms', 'tasks_test'])
