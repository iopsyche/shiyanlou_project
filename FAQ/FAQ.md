## 问题可以随意添加, 解决问题的小组加分, (自问自答的不加分)

###0 规则中有bug吗? 

问题1: 每个小组只要不添加问题, 就可以保证别的小组不得分了

问题2: 我们回答问题了, 就算赢得了小组pk, 也只不过是一瓶饮料

解答: 换一个思维方式, 如果大家都问问题, 相互解决问题, 大家都有很高的分数, 能解决问题也是成长, 我们在一起就是为了学习的. 世界很大, 眼界放宽一点, 社会上的对手很多. 

###0 问答书写格式

问题: 问答书写格式

解答: 标题要编号, 标题需要简明扼要的写出是什么类型的问题, 问题要详细描述, 解答也要详细, 标题,问题,解答前要空行

###1 FAQ是什么

问题: FAQ是什么?

解答: FAQ是英文Frequently Asked Questions的缩写，中文意思就是“经常问到的问题”，或者更通俗地叫做“常见问题解答”。

###2 mysql 复合索引

问题: abc复合索引 为啥where a=3 and b like 'k%' and c=4，b是范围，用到了abc,而 where a=3 and b>4 and c=5，b也是范围，c失效了

解答: b 是字符型的类型,  b > 4 是需要隐式转换的, 转换会使索引失效.
索引失效的几种情况:https://blog.csdn.net/qq_24861509/article/details/108036326

###3 实际开发中也可以用容联云通讯的多吗

问题1: 实际开发中也可以用容联云通讯的多吗

解答: 很多, 因为开发简单, 服务好.

###4 celery 导包详解, 解答人:张德阳

问题1: celery 导包详解

解答: 解答

###5 windows celery 执行任务报错 解答人: 韩磊

问题: ValueError: not enough values to unpack (expected 3, got 0)

解答: celery worker -A celery_name --loglevel=info --pool=solo #windows命令

###6 本地域名解析配置

问题1: 如何把mysyl.com 解析到本地 127.0.0.1

解答: 1. windows中修改C:\Windows\System32\drivers\etc\hosts 文件, 增加127.0.0.1 mysyl.com
     2. linux中修改 /etc/hosts 文件, 增加 127.0.0.1  mysyl.com

###7 django2.2解决Object of type Decimal is not JSON serializabl

问题1: Object of type Decimal is not JSON serializabl


解答: 
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        super(DecimalEncoder, self).default(o)
        
json.dumps(goods.price, cls=DecimalEncoder)

###8 启动工程, 报没有明确声明app_label

问题1: Model class course.models.CourseType doesn't declare an explicit app_label and isn't in an application in INSTALLED_APPS.

问题2: 问题

解答: 先查看 INSTALLED_APPS 中的应用都安装没有, 如果有没有安装的, 先安装好. 这个列表中的任何一个应用没有安装或者找不到, 都会影响整个列表的可用性. 导致出现没有 app_label的问题.

###9 安装 django-haystack 报错

问题1: Could not find a version that satisfies the requirement setuptools_scm

问题2: 问题

解答: 先安装 pip install setuptools_scm

###10 无法启动fdsf的storage

问题1: docker 启动 storage 服务, 无法启动起来

问题2: 问题

解答: 删除  /var/fdfs/storage/data/ 文件夹下的  *.pid文件

###编号 标题

问题1: 问题

问题2: 问题

解答: 解答
